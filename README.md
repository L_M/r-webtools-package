## R "webtools" package

A set of useful web tools to improve your productivity.

- **dsearch** Search for a dataset in all CRAN packages
- **dinfo** Get dataset info on a package (all of CRAN) and fetch/load the dataset/s
- **dfetch** Fetch/load a dataset - see dsearch() and dinfo()
- **ddg** Duckduckgo Instant-search
- **guid** Get a unique GUID
- **sharebin**	"Paste" a file for web sharing
- **countries**	Get useful info on all countries
- **yada**	Get a random Seinfeld quote
- **yadayada**	Seinfeld quote guessing game

### Installation

`install.packages("https://l_m.gitlab.io/r-webtools-package/webtools_0.7.13.tar.gz")`
